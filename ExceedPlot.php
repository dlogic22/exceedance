<?php

class ExceedancePlot
{
    public $dataPoints;
    protected $allX;
    
    // Takes an array of arrays where the nested arrays represent x,y coordinates.
    // Example: [ [0,1], [2,8], [5,10] ]
    function __construct($dataPoints)
    {
        $this->dataPoints = $dataPoints;

        foreach ($dataPoints as $key => $value) {
            $this->allX []= $key;
        }
    }

    /**
     * Returns the indecies for X and Y respectively in an array.
     * [X, Y] indecies.
     **/
    function getIndecies($x)
    {
        // Handle if X is 0
        if ($x == 0) {
            return [0,1];
        }

        $array_length = count($this->allX); 
        $last         = $array_length - 1;

        // Handle if X is the last element
        if ($x == $this->allX[$last]) {
            return [$last-1, $last];
        }

        // Find pair of Xs that X is between
        $i = 0;	
        for ($i=0; $i <= $last; $i++) {
            if ($x <= $this->allX[$i]) {
                break;
            }
        }

        if ($i > $last) { 
            return [$last-1, $last];
        }

        return [$i-1, $i];
    }

    // Get the slope for a given X within the exceedance pairs
    function getSlope($x)
    {
        $indecies = $this->getIndecies($x);

        $x1 = $this->dataPoints[$indecies[0]][0];
        $x2 = $this->dataPoints[$indecies[1]][0];
        $y1 = $this->dataPoints[$indecies[0]][1];
        $y2 = $this->dataPoints[$indecies[1]][1];

        return ($y2 - $y1) / ($x2 - $x1);
    }

    // Get the offset for a given X within the exceedance pairs
    function getOffset($x)
    {
        $indecies = $this->getIndecies($x);

        $x1 = $this->dataPoints[$indecies[0]][0];
        $y1 = $this->dataPoints[$indecies[0]][1];

        $slope = $this->getSlope($x);

        return $y1 - $slope * $x1; 
    }

    // Compute the y value given x inside a set of exceedance pairs
    function formula($x) 
    {
        $slope  = $this->getSlope($x);
        $offset = $this->getOffset($x);

        return $slope * $x + $offset;
    }

    // Check if a data point exceeds a set of exceedance points
    function exceeds($dataPoint)
    {
        $x = $dataPoint[0];
        $y = $dataPoint[1];

        if (abs($y) > $this->formula($x)) {

           // echo "Exceedance: $x, " . abs($y) . " \n";
           // echo "vs: " . $this->formula($x) . "\n";
            
            return true;
        }

        return false;
    }

    public function checkPairs($pairs)
    {
        foreach ($pairs as $pair) {
            if ($this->exceeds(array_reverse($pair))) {
                return true;
            }
        }

        return false;
    }
}
